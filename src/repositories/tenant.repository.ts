import { inject } from '@loopback/core';
import { DefaultCrudRepository } from '@loopback/repository';
import { MongoDataSource } from '../datasources';
import { Tenant, TenantRelations } from '../models';

const dsCache: {central: MongoDataSource | undefined} = {
  central: undefined,
};

const dsConfig = {
  name: 'mongo',
  connector: 'mongodb',
  allowExtendedOperators: true,
  url: `mongodb://${process.env.MONGODB_AUTH_USERNAME}:${process.env.MONGODB_AUTH_PASSWORD}@${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_CENTRAL_DATABASE}?authSource=admin&useNewUrlParser=true&useUnifiedTopology=true&maxPoolSize=10&socketTimeoutMS=360000&connectTimeoutMS=10000&maxIdleTimeMS=300000`
}

export class TenantRepository extends DefaultCrudRepository<
  Tenant,
  typeof Tenant.prototype.id,
  TenantRelations
> {
  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource,
  ) {
    // to use pre-defined mongodb database
    if(!dsCache.central) {
      dsCache.central = new MongoDataSource(dsConfig);
    }
    super(Tenant, dsCache.central);
  }

  /**
   * Find tennat instance by property 'code'
   * @param {string} code
   * @returns {Tenant}
   */
  async findByCode(code: string): Promise<Tenant> {
    const tennats = await this.find({ where: { code: {'eq': code}, isDeleted: false } });
    if (!tennats.length) throw new Error(`A valid tenant is not found by code '${code}'`);
    return tennats[0];
  }

  /**
   * Find tenant instance by property 'apps' which is an array
   * @param {string} app 
   * @returns {Tenant}
   */
  async findByApp(app: string): Promise<Tenant> {
    const tennats = await this.find({ where: { apps: app, isDeleted: false } });
    if (!tennats.length) throw new Error(`A valid tenant is not found by app '${app}'`);
    return tennats[0];
  }
}
