import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

export const mongodbConfig = {
  name: 'mongo',
  connector: 'mongodb',
  allowExtendedOperators: true,
  url: `mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/trap`,
  // url: `mongodb://${process.env.MONGODB_AUTH_USERNAME}:${process.env.MONGODB_AUTH_PASSWORD}@${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/trap?authSource=admin`,
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class MongoDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'mongo';
  static readonly defaultConfig = mongodbConfig;

  constructor(
    @inject('datasources.config.mongo', {optional: true})
    dsConfig: object = mongodbConfig,
  ) {
    super(dsConfig);
  }
}
