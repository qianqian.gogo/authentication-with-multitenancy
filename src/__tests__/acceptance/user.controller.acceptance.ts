import {Client, expect} from '@loopback/testlab';
import {AuthenticationWithMultitenancyApplication} from '../..';
import {setupApplication} from './test-helper';

describe('UserController', () => {
  let app: AuthenticationWithMultitenancyApplication;
  let client: Client;
  let token: string | undefined;

  before('setupApplication', async () => {
    ({app, client} = await setupApplication());
  });

  after(async () => {
    await app.stop();
  });

  it('invokes POST /login with unauthorized error', async () => {
    await client.post('/users/login').set({'x-tenant-id': 'TEST', 'x-api-token': 'TEST'}).expect(401);
  });

  it('invokes POST /login', async () => {
    const res = await client.post('/users/login').set({'x-tenant-id': 'TEST', 'x-api-token': process.env.API_TOKEN}).expect(200);
    expect(res.body.token).to.be.a.String();
    token = res.body.token;
  });

  it('invokes POST /token/refresh', async () => {
    const res = await client.post('/users/token/refresh').auth(<string>token, { type: "bearer" }).expect(200);
    expect(res.body.token).to.be.a.String();
    token = res.body.token;
  });

  it('invokes POST /logout', async () => {
    await client.post('/users/logout').auth(<string>token, { type: "bearer" }).expect(204);
  });
});
