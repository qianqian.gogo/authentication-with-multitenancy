import { JwtService, UserJwt } from "../../../services";
import { promisify } from 'util';
const jwt = require('jsonwebtoken');
const signAsync = promisify(jwt.sign);
const verifyAsync = promisify(jwt.verify);

const jwtSecret = process.env.JWT_SECRET ?? 'test';
const jwtExpiresIn = process.env.JWT_TOKEN_EXPIRES_IN ?? '28800';

export class MockJwtService extends JwtService {
    constructor() {
        super(jwtSecret, jwtExpiresIn);
    }

    async generateToken(user: UserJwt): Promise<string> {
         // Generate a JSON Web Token
        try {
            return signAsync(user, jwtSecret, {expiresIn: Number(jwtExpiresIn)});
        }
        catch (error) {
            throw new Error('JWT encoding error.');
        }
    }

    async verifyToken(token: string): Promise<UserJwt> {
        try {
            // decode user payload from token
            return await verifyAsync(token, jwtSecret) as UserJwt;
        } catch (error) {
            throw new Error('Verify JWT error.');
        }
    }
}