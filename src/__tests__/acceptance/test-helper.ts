import {AuthenticationWithMultitenancyApplication} from '../..';
import {
  createRestAppClient,
  givenHttpServerConfig,
  Client,
} from '@loopback/testlab';
import { JwtService } from '../../services';

export async function setupApplication(): Promise<AppWithClient> {
  const restConfig = givenHttpServerConfig({
    // Customize the server configuration here.
    // Empty values (undefined, '') will be ignored by the helper.
    //
    // host: process.env.HOST,
    // port: +process.env.PORT,
  });

  const app = new AuthenticationWithMultitenancyApplication({
    rest: restConfig,
  });

  await app.boot();
  await app.start();

  const client = createRestAppClient(app);

  return {app, client};
}

export interface AppWithClient {
  app: AuthenticationWithMultitenancyApplication;
  client: Client;
}

export async function mockServices(
  app: AuthenticationWithMultitenancyApplication,
): Promise<void> {
  // Bind the service that makes external REST call to a mock service.
  app.bind('services.JwtService').toClass(JwtService);
}
