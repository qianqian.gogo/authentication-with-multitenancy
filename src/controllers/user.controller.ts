import { authenticate } from '@loopback/authentication';
import { inject } from '@loopback/core';
import { HttpErrors, param, post, response } from '@loopback/rest';
import { securityId} from '@loopback/security';
import { MultiTenancyBindings, Tenant, UserRole } from '../components/multi-tenancy';
import { JwtService, UserJwt } from '../services';

export class UserController {
  constructor(
    @inject('services.JwtService')
    public jwtService: JwtService,
    @inject(MultiTenancyBindings.CURRENT_TENANT, {optional: true})
    private tenant: Tenant,
    @inject(MultiTenancyBindings.JSON_WEB_TOKEN_PAYLOAD, {optional: true})
    private jwt: UserJwt
  ) { }

  @post('/users/login')
  @response(200, {
    description: 'login',
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            token: {
              type: 'string',
            },
          },
        },
      },
    },
  })
  async login(
    @param.header.string('x-api-token', {required: true}) apiToken: string,
    @param.header.string('x-tenant-id', {required: true}) tenantId: string,
  ): Promise<{token: string;}> {
    if (apiToken !== process.env.API_TOKEN) throw new HttpErrors[401];

    const userId = String(new Date().getTime());

    const userJwt = convertToUserJwt(userId, this.tenant, [UserRole.Admin], {}) as UserJwt;
    const token = await this.jwtService.generateToken(userJwt);
    return { token };
  }

  @authenticate('jwt')
  @post('/users/logout')
  @response(204, {
    description: 'User logout successfully'
  })
  async logout(): Promise<void> {
    // destroy JWT
  }

  @authenticate('jwt')
  @post('/users/token/refresh')
  @response(200, {
    description: 'JWT refreshed successfully',
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            token: {
              type: 'string',
            },
          },
        },
      },
    },
  })
  async refreshToken(): Promise<{token: string}> {
    const { roles, options,  jwi: userId} = this.jwt;
    const userJwt = convertToUserJwt(userId, this.tenant, roles, options) as UserJwt;
    const token = await this.jwtService.generateToken(userJwt);
    return { token }
  }
}

const convertToUserJwt = (
  userId: string,
  tenant: Tenant,
  roles: UserRole[],
  options: object = {}
): UserJwt => {
  return {
    [securityId]: userId,
    jwi: userId,
    iss: '',
    sub: userId,
    aud: '',
    tenant,
    roles,
    options,
  };
}
