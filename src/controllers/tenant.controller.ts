import { authenticate } from '@loopback/authentication';
import { authorize } from '@loopback/authorization';
import {
  Filter,
  repository
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
  response
} from '@loopback/rest';
import { UserRole } from '../components/multi-tenancy';
import { basicAuthorization } from '../middlewares/auth.middleware';
import {Tenant} from '../models';
import {TenantRepository} from '../repositories';

export class TenantController {
  constructor(
    @repository(TenantRepository)
    public tenantRepository : TenantRepository,
  ) {}

  @authenticate('jwt')
  @authorize({
    allowedRoles: [UserRole.Admin],
    voters: [basicAuthorization],
  })
  @post('/tenants')
  @response(200, {
    description: 'Tenant model instance',
    content: {'application/json': {schema: getModelSchemaRef(Tenant)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Tenant, {
            title: 'NewTenant',
            exclude: ['id'],
          }),
        },
      },
    })
    tenant: Omit<Tenant, 'id'>,
  ): Promise<Tenant> {
    return this.tenantRepository.create(tenant);
  }

  @authenticate('jwt')
  @authorize({
    allowedRoles: [UserRole.Admin],
    voters: [basicAuthorization],
  })
  @get('/tenants')
  @response(200, {
    description: 'Array of Tenant model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Tenant, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Tenant) filter?: Filter<Tenant>,
  ): Promise<Tenant[]> {
    return this.tenantRepository.find(filter);
  }

  @authenticate('jwt')
  @authorize({
    allowedRoles: [UserRole.Admin],
    voters: [basicAuthorization],
  })
  @get('/tenants/{code}')
  @response(200, {
    description: 'Tenant model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Tenant, {includeRelations: true}),
      },
    },
  })
  async findByCode(
    @param.path.string('code') code: string
  ): Promise<Tenant> {
    return this.tenantRepository.findByCode(code);
  }

  @authenticate('jwt')
  @authorize({
    allowedRoles: [UserRole.Admin],
    voters: [basicAuthorization],
  })
  @patch('/tenants/{id}')
  @response(204, {
    description: 'Tenant PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Tenant, {partial: true}),
        },
      },
    })
    tenant: Tenant,
  ): Promise<void> {
    await this.tenantRepository.updateById(id, tenant);
  }

  @authenticate('jwt')
  @authorize({
    allowedRoles: [UserRole.Admin],
    voters: [basicAuthorization],
  })
  @del('/tenants/{id}')
  @response(204, {
    description: 'Tenant DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.tenantRepository.deleteById(id);
  }
}
