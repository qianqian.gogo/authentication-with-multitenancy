import { TokenService } from '@loopback/authentication';
import { TokenServiceBindings } from '@loopback/authentication-jwt';
import { BindingScope, inject, injectable } from '@loopback/core';
import { UserProfile } from '@loopback/security';
import { promisify } from 'util';
import { UserRole } from '../components/multi-tenancy';

const jwt = require('jsonwebtoken');
const signAsync = promisify(jwt.sign);
const verifyAsync = promisify(jwt.verify);

export interface UserJwt extends UserProfile {
  iss: string;    // Issuer of the JWT
  aud: string;    // Recipient for which the JWT is intended
  sub: string;     // Subject of the JWT (the user)
  jwi: string;    // Unique identifier; can be used to prevent the JWT from being replayed (allows a token to be used only once): sessionId
  tenant: {
    id: string;   // Tenant of the JWT
  };
  roles: UserRole[];
  [attribute: string]: any;
}

@injectable({scope: BindingScope.TRANSIENT})
export class JwtService implements TokenService {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SECRET)
    private jwtSecret: string,
    @inject(TokenServiceBindings.TOKEN_EXPIRES_IN)
    private jwtExpiresIn: string,
  ) { }

  /**
   * Encode JWT using user payload
   * @param {UserJwt} user
   * @param {number} expiresIn (optional)
   * @returns {string} encoded JWT based on user data
   */
  async generateToken(user: UserJwt, expiresIn?: number): Promise<string> {
    if (!user) throw new Error('JWT payload is required.');
    // Generate a JSON Web Token
    try {
      return signAsync(user, this.jwtSecret, {
        expiresIn: expiresIn ?? Number(this.jwtExpiresIn),
      });
    }
    catch (error) {
      throw new Error('JWT encoding error.');
    }
  }

  /**
   * Verify JWT and decode it
   * @param {string} token
   * @returns {UserJwt} the decoded user payload
   */
  async verifyToken(token: string): Promise<UserJwt> {
    if (!token) throw new Error('JWT is required.');

    try {
      // decode user payload from token
      return await verifyAsync(token, this.jwtSecret) as UserJwt;

    } catch (error) {
      throw new Error('Verify JWT error.');
    }
  }
}
