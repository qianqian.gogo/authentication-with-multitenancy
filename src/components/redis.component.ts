import {
  Binding,
  BindingKey,
  Component, CoreBindings,
  inject, LifeCycleObserver
} from '@loopback/core';
import {RestApplication} from '@loopback/rest';
import {createClient} from 'redis';

const configFromEnv = {
  url: process.env.REDIS_URL,
};

export type RedisClientType = ReturnType<typeof createClient>;

export namespace RedisBindings {
  export const REDIS_CLIENT = BindingKey.create<RedisClientType>(
    'redis-component.redisClient',
  );
}

export class RedisComponent implements Component, LifeCycleObserver {
  status = 'not-initialized';
  initialized = false;
  private readonly redisClient = createClient(configFromEnv);

  bindings: Binding[];

  constructor(
    @inject(CoreBindings.APPLICATION_INSTANCE)
    private app: RestApplication,
  ) { }

  async init() {
    // Contribute bindings via `init`
    this.app.bind(RedisBindings.REDIS_CLIENT).to(this.redisClient);

    this.status = 'initialized';
    this.initialized = true;
  }

  async start() {
    await this.redisClient.connect();
    this.status = 'started';
  }

  async stop() {
    await this.redisClient.disconnect();
    this.status = 'stopped';
  }
}
