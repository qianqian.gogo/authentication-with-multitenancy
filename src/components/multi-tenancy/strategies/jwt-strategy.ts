import {RequestContext} from '@loopback/rest';
import {MultiTenancyStrategy, Tenant} from '../types';
import {BaseStrategy} from './base-strategy';

/**
 * Use jwt token to identify the tenant id
 */
export class JWTStrategy extends BaseStrategy implements MultiTenancyStrategy {
  name = 'jwt';

  async identifyTenant(requestContext: RequestContext) {
    const authorization = requestContext.request.headers[
      'authorization'
    ] as string;
    if (authorization?.startsWith('Bearer ')) {
      //split the string into 2 parts : 'Bearer ' and the `xxx.yyy.zzz`
      const parts = authorization.split(' ');
      const token = parts[1];
      // decode token
      const json = await this.jwtService.verifyToken(token);
      // get tenant
      const tenant = !json?.tenant ? null : json.tenant as Tenant;
      return {tenant, jwtToken: token, userJwt: json};
    }
  }
}
