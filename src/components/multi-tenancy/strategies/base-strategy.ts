import {CoreBindings, inject} from '@loopback/core';
import {juggler} from '@loopback/repository';
import {RequestContext, RestApplication} from '@loopback/rest';
import {JwtService} from '../../../services';
import {Tenant} from '../types';
import {mongodbConfig} from '../../../datasources';

export abstract class BaseStrategy {
  constructor(
    @inject('services.JwtService')
    public jwtService: JwtService,
    @inject(CoreBindings.APPLICATION_INSTANCE)
    private app: RestApplication,
    @inject('datasources.config.mongo', {optional: true})
    private dsConfig = mongodbConfig,
  ) { }

  async bindResources(
    requestContext: RequestContext,
    tenant: Tenant,
  ): Promise<void> {
    const dsName = `datasources.${tenant.id}`;
    // console.log('dsName > ', dsName);

    if (!requestContext?.isBound(dsName)) {
      await this.setupDataSource(tenant, dsName);
    }

    requestContext
      .bind('datasources.mongo')
      .toAlias(dsName);
    }

    private async setupDataSource(tenant: Tenant, dsName: string): Promise<void> {
      const resolvedConfig = {
        ...this.dsConfig,
        name: tenant.id,
        url: this.dsConfig.url.replace('/trap', `/${tenant.id}`),
      };

      const ds = new TenantDataSource(resolvedConfig);
      try {
        await ds.connect();
      } catch(err) {
        console.log(`[${dsName}] ds.connect.error`, err)
      }

      // Important! We need to bind the datasource to the root (application-level)
      // context to reuse the same datasource instance for all requests.
      this.app.bind(dsName).to(ds).tag({
        name: tenant.id,
        type: 'datasource',
        namespace: 'datasources',
      });
    }
}

export class TenantDataSource extends juggler.DataSource {
  // no static members like `dataSourceName`
  // constructor is not needed, we can use the inherited one.
  // start/stop methods are needed, I am skipping them for brevity
}
