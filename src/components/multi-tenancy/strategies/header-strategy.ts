import {RequestContext} from '@loopback/rest';
import {MultiTenancyStrategy, Tenant} from '../types';
import {BaseStrategy} from './base-strategy';

/**
 * Use `x-tenant-id` http header to identify the tenant id
 */
export class HeaderStrategy extends BaseStrategy implements MultiTenancyStrategy {
  name = 'header';

  async identifyTenant(requestContext: RequestContext) {
    const tenantId = requestContext.request.headers['x-tenant-id'] as string;
    const tenant = !tenantId ? null : {id: tenantId} as Tenant;

    return {tenant};
  }
}
