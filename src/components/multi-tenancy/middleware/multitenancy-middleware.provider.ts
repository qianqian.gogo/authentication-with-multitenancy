import {
  config,
  ContextTags,
  extensionPoint,
  extensions,
  Getter,
  Provider
} from '@loopback/core';
import {asMiddleware, Middleware, RequestContext, RestMiddlewareGroups} from '@loopback/rest';
import {MultiTenancyBindings, MULTI_TENANCY_STRATEGIES} from '../keys';
import {MultiTenancyMiddlewareOptions, MultiTenancyStrategy} from '../types';

/**
 * Provides the multi-tenancy action for a sequence
 */
@extensionPoint(
  MULTI_TENANCY_STRATEGIES,
  {
    tags: {
      [ContextTags.KEY]: MultiTenancyBindings.MIDDLEWARE,
    },
  },
  asMiddleware({
    group: 'tenancy',
    downstreamGroups: RestMiddlewareGroups.CORS,
  }),
)
export class MultiTenancyMiddlewareProvider implements Provider<Middleware> {
  constructor(
    @extensions()
    private readonly getMultiTenancyStrategies: Getter<MultiTenancyStrategy[]>,
    @config()
    private options: MultiTenancyMiddlewareOptions = {
      strategyNames: ['jwt', 'header'],
    },
  ) { }

  value(): Middleware {
    return async (ctx, next) => {
      const { response } = ctx;
      try {
        await this.action(ctx as RequestContext);
      }
      catch (err) {
        if (err?.name.startsWith('JWT_SERVICE')) {
          response.status(err.code);
          return response.send({
            error: {
              message: err.message,
              name: err.name.split('.')?.[1],
              statusCode: err.code
            }
          });
        }
      }
      // if no exception, continue
      return next();
    };
  }

  /**
   * The implementation of authenticate() sequence action.
   * @param {RequestContext} requestCtx - The incoming request provided by the REST layer
   */
  async action(requestCtx: RequestContext) {
    if(!requestCtx.request.headers.authorization
        && requestCtx.request.query['access_token']) {
      requestCtx.request.headers.authorization = 'Bearer ' + requestCtx.request.query['access_token']
    }
    const tenancy = await this.identifyTenancy(requestCtx);
    if (!tenancy) return;
    // bind to context
    if (tenancy.jwtToken) {
      requestCtx.bind(MultiTenancyBindings.JSON_WEB_TOKEN).to(tenancy.jwtToken);
    }
    if (tenancy.userJwt) {
      requestCtx.bind(MultiTenancyBindings.JSON_WEB_TOKEN_PAYLOAD).to(tenancy.userJwt);
    }
    if (tenancy.tenant) {
      requestCtx.bind(MultiTenancyBindings.CURRENT_TENANT).to(tenancy.tenant);
      await tenancy.strategy.bindResources(requestCtx, tenancy.tenant);
      return tenancy.tenant;
    }
  }

  /**
   * To identify tenant and get strategy
   * @param {RequestContext} requestCtx
   * @returns {tenant: Tenant; strategy: MultiTenancyStrategy | undefined}
   */
  private async identifyTenancy(requestCtx: RequestContext) {
    const strategyNames = this.options.strategyNames;
    let strategies = await this.getMultiTenancyStrategies();
    strategies = strategies
      .filter(s => strategyNames.includes(s.name))
      .sort((a, b) => {
        return strategyNames.indexOf(a.name) - strategyNames.indexOf(b.name);
      });

    for (const strategy of strategies) {
      const data = await strategy.identifyTenant(requestCtx);
      if (data) return {...data, strategy};
    }
  }
}
