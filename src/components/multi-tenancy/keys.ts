import {BindingKey} from '@loopback/core';
import {Middleware} from '@loopback/rest';
import { UserJwt } from '../../services';
import {Tenant} from './types';

export namespace MultiTenancyBindings {
  export const MIDDLEWARE = BindingKey.create<Middleware>(
    'middleware.multi-tenancy',
  );

  export const JSON_WEB_TOKEN = BindingKey.create<string>(
    'multi-tenancy.jsonWebToken',
  );

  export const CURRENT_TENANT = BindingKey.create<Tenant>(
    'multi-tenancy.currentTenant',
  );

  export const JSON_WEB_TOKEN_PAYLOAD = BindingKey.create<UserJwt>(
    'multi-tenancy.jwtPayload',
  );
}

export const MULTI_TENANCY_STRATEGIES = 'multi-tenancy.strategies';
