import {BootMixin} from '@loopback/boot';
import {ApplicationConfig} from '@loopback/core';
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from '@loopback/rest-explorer';
import {RepositoryMixin} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {ServiceMixin} from '@loopback/service-proxy';
import path from 'path';
import {MySequence} from './sequence';

import { AuthenticationComponent } from '@loopback/authentication';
import {
  JWTAuthenticationComponent,
  TokenServiceBindings
} from '@loopback/authentication-jwt';
import { RedisComponent } from './components/redis.component';
import { MultiTenancyComponent } from './components/multi-tenancy/component';
import { JwtService } from './services';
import { AuthorizationComponent } from '@loopback/authorization';

export {ApplicationConfig};

export class AuthenticationWithMultitenancyApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Mount authentication system
    this.component(AuthenticationComponent);
    this.component(AuthorizationComponent);

    // Mount jwt component
    this.component(JWTAuthenticationComponent);

    // Mount multitenancy component
    this.component(MultiTenancyComponent);

    // Mount redis component
    this.component(RedisComponent);

    this.setUpBindings();

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
  }

  setUpBindings(): void {
    // bind Jwtv service
    this.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JwtService);

    // Use JWT secret from JWT_SECRET environment variable, otherwise use DEFAULT
    const jwtSecret = process.env.JWT_SECRET;
    if (jwtSecret) this.bind(TokenServiceBindings.TOKEN_SECRET).to(jwtSecret);

    // Use JWT expires in from JWT_TOKEN_EXPIRES_IN environment variable, otherwise use DEFAUTL (6 hours)
    const jwtExpiresIn = process.env.JWT_TOKEN_EXPIRES_IN ?? "28800";
    if (jwtExpiresIn) this.bind(TokenServiceBindings.TOKEN_EXPIRES_IN).to(jwtExpiresIn);
  }
}
