import {
  AuthorizationContext, 
  AuthorizationDecision, 
  AuthorizationMetadata
} from '@loopback/authorization';
import {securityId, UserProfile} from '@loopback/security';
import _ from 'lodash';
import { UserRole } from '../components/multi-tenancy';

// Instance level authorizer
// Can be also registered as an authorizer, depends on users' need.
export async function basicAuthorization(
  authorizationCtx: AuthorizationContext,
  metadata: AuthorizationMetadata,
): Promise<AuthorizationDecision> {
  // No access if authorization details are missing
  let currentUser: UserProfile;

  if (authorizationCtx.principals.length > 0) {
    const user = _.pick(authorizationCtx.principals[0], [
      'jwi',
      'sub',
      'roles',
    ]);
    currentUser = {
        [securityId]: user.jwi, 
        name: user.sub, 
        roles: user.roles ?? []
    };
  } else {
    return AuthorizationDecision.DENY;
  }

  // if user has no roles, then DENY
  if (!currentUser.roles.length) {
    return AuthorizationDecision.DENY;
  }

  // Authorize everything that does not have a allowedRoles property
  if (!metadata.allowedRoles) {
    return AuthorizationDecision.ALLOW;
  }

  let roleIsAllowed = false;
  if (currentUser.roles.find((role: UserRole) => metadata.allowedRoles!.includes(role))) {
    roleIsAllowed = true;
  }
  
  if (!roleIsAllowed) {
    return AuthorizationDecision.DENY;
  }

  return AuthorizationDecision.ALLOW;
}