module.exports = {
  extends: '@loopback/eslint-config',
  rules: {
    "@typescript-eslint/naming-convention": [
      "warn",
      {
        selector: "variable",
        format: ["camelCase", "UPPER_CASE", "PascalCase"]
      },
    ],
    "@typescript-eslint/return-await": ["warn"],
    "@typescript-eslint/no-explicit-any": ["warn"]
  }
};